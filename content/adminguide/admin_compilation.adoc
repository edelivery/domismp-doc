== Compiling SMP

=== Compilation Prerequisites

.Supported Operating System Platform

The eDelivery SMP can be built on the following OS platforms:

* Windows Workstation & Server
* Linux platform

.Software Requirements

The following software components are required on the target system:

* Java Development Kit environment (JDK), version 8: +
http://www.oracle.com/technetwork/java/javase/downloads/index.html
* Maven 3.6 and above (https://maven.apache.org/download.cgi)
* GIT (optional: Git is only used to download the project sources but
these sources can be downloaded from any system having Git installed and
then just copied manually on the compilation platform).

[[smp_downloadcode]]
=== Downloading the Source Code

image:image19.png[width=595,height=494,role=left]

The source code of SMP is freely available and can be downloaded from the +
https://ec.europa.eu/digital-building-blocks/code/projects/EDELIVERY/repos/smp/browse[SMP Source Code Repository,window=_blank].

{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +

.~URL~
[%collapsible%open]
====
~*SMP*~ ~*Source*~ ~*Code*~ ~*Repository:*~ +
https://ec.europa.eu/digital-building-blocks/code/projects/EDELIVERY/repos/smp/browse
====

=== Compiling SMP Source Code

1. Create a new `/comp_dir` temporary directory.

2. From `/comp_dir`, execute:
+
[source,bash]
----
git clone https://ec.europa.eu/digital-building-blocks/code/scm/edelivery/smp.git
----

3. Go to the newly created `/smp` directory.
+
Using a ls command in the above-mentioned directory renders the following contents:
+
[source, text]
----
pom.xml README.md smp-api smp-parent-pom smp-server-library smp-soapui-tests smp-webapp
----

4. Start the compilation by executing the following command:
+
[source, text]
----
mvn clean install -DskipTests
----

5. A successful compilation will result with the following:
+
[source, text]
----
mvn clean install -DskipTests
----
+
.Expected Console Output
[Source, text]
----
[INFO] Scanning for projects…
/..
../

[INFO] Installing /home/smpcomp/smp/smp/pom.xml to
/home/smpcomp/.m2/repository/eu/europa/ec/smp/3.X/smp-3.X.pom

[INFO]
------------------------------------------------------------------------
[INFO] Reactor Summary:
[INFO]
[INFO] smp-angular ........................................ SUCCESS [132.375 s]
[INFO] smp-api ............................................ SUCCESS [32.375 s]
[INFO] smp-server-library ................................. SUCCESS [02:01 min]
[INFO] smp-webapp ......................................... SUCCESS [23.314 s]
[INFO] SMP Builder POM .................................... SUCCESS [2.222 s]
[INFO]
------------------------------------------------------------------------

[INFO] BUILD SUCCESS

[INFO]
------------------------------------------------------------------------
[INFO] Total time: 03:00 min
[INFO] Finished at: 2017-06-08T11:35:27+02:00
[INFO] Final Memory: 61M/726M
[INFO]
------------------------------------------------------------------------
----

As a result the web application, `smp.war` , is created in the `/smp-webapp/target/` directory.

Using a `ls` command in the directory mentioned above renders the following contents:

[source, text]
----
smp-X smp.war classes generated-sources generated-test-sources maven-status test-classes webapp-classes

----

