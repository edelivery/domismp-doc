== Logging Configuration

=== Logging properties

The SMP logging properties are defined in the
./WEB-INF/classes/logback.xml file embedded in the SMP `.war` file.

It is possible to modify the configuration of the logs by editing the
embedded `logback.xml` or by defining new logback file in
*smp.config.properties* file as example:

`log.configuration.file=/opt/apache-tomcat-8.5.30/smp/logback.xml`

//Text extraction from image12, using online API
.Sample `logback.xml` file
[source,xml]
----
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
    <!-- pattern definition -->
    <property name="encoderPattern" value="%d{IS08601} [%X{smp_user}] [%X{smp_session_id}] [%X{smp_request_id}] [%thread] 5p %c{1}: %L - %m%n" scope="global"/>
    <property name="consolePattern" value="%d{IS08601} [%X{smp_user}] [%X{smp_session_id}] [%X{smp_request_id}] [%thread] %5p %c{1}: %L - %m%n" scope="global"/>

        <appender name="file" class="ch.qos.logback.core.rolling. RollingFileAppender">
            <file>${log.folder: -logs}/edelivery-smp.log</file>
            <filter class="ch.qos.logback.core.filter.EvaluatorFilter">
                <evaluator class="ch.qos.logback.classic.boolex. OnMarkerEvaluator">
                    <marker>SECURITY</marker>
                    <marker>BUSINESS</marker>
                </evaluator>
                <onMismatch>NEUTRAL</onMismatch>
                <onMatch>DENY</onMatch>
            </filter>
            <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRolling Policy">
                <!-- rollover daily -->
                <fileNamePattern>
                    ${log.folder: -logs}/edelivery-smp-%d{yyyy-MM-dd}. %i.log
                </fileNamePattern>
                <!-- each file should be at most 30MB, keep 60 days worth of history,
                but at most 20GB -->
                <maxFileSize>30MB</maxFileSize>
                <maxHistory>60</maxHistory>
                <totalSizeCap>20GB</totalSizeCap>
            </rollingPolicy>
            <encoder>
                <pattern>${encoderPattern}</pattern>
            </encoder>
        </appender>
        <appender name="stdout" class="ch.qos.logback.core.ConsoleAppender">
            <Target>System.out</Target>
            <encoder>
                <pattern>${consolePattern}</pattern>
            </encoder>
        </appender>

        <logger name="eu.europa.ec.edelivery.smp" level="INFO" />
        <logger name="org.springframework.security.cas" level="DEBUG" />
        <root level="DEBUG">
            <appender-ref ref="file"/>
            <appender-ref ref="stdout"/>
        </root>
    </configuration>
----

More details on how to configure logback can be found at: +
`https://logback.qos.ch/documentation.html`

