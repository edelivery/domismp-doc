[[smp_admin_usermanagement]]
== SMP User Management

The DomiSMP has two user application roles:

* System Admin: the role allows user to modify DomiSMP system management
and settings such as: Domain management, User management, Truststore
management, Key management, DomiSMP configuration, etc.
* User: this role allows the user to log into the DomiSMP.

The user can have additional permissions on editing DomiSMP entities
when they are assigned to the Resource, Groups, and Domains. Please read
the following chapter for more details.

=== Domain, Group and Resources

The DomiSMP supports 3-layer security realms.

Resource:: _the most basic unit._ +
The Resource is identified by the unique ID, which is part of the URL of the resource as example:
+
`http://localhost/smp/resource-identifier`
+
A Resource example is the _Service Group_ document from the Oasis SMP specification.
The user can be a Resource member with *Admin* or *Viewer* membership roles.
If the user has an Admin membership role,
it can modify resource document(s) and manage the resource memberships.
If the user has role Viewer, it can view/read the Resource if the
Resource has visibility set to: `Private`.

Group:: _a cluster of resources managed by the dedicated group administrators._ +
The group admin(s) can create and delete the resource,
but _only_ the resource admins can modify data/documents for the
resource. The user can be a Group member with *Admin* or *Viewer*
membership roles. With Admin group membership, the user can create and
delete group resources. If the user has group role Viewer, it can
view/read the Resources if the Group has visibility set to: “Private”.

Domain:: _the top layer._ +
It indicates the business purpose of the network of participants,
such as invoice exchange, Health Records message exchanges, etc.
The Domain usually has a domain owner who handles participant interoperability,
defining message types, network authentication, and authorization methods
such as Certificate PKI, Identity Service providers, etc.
+
In DomiSMP 5.0, the user with a Domain Admin role can create domain groups and assign users to them.

image::image11.png[width=524,height=171]

The provided database script creates the following users:

//.Figure 5 - DomiSMP UI: Property settings page

[caption="" title="Default Users created by DB Script"]
[width="60%",cols="30%,30%,40%",options="header"]
|===
|*User Name* |*Role* |*Default Password* +
m|system m|SYSTEM_ADMIN m|123456
m|user m|USER m|123456

3+a|
IMPORTANT: Change these default passwords immediately for security reasons.
|===

=== User Roles

The following DomiSMP users can be of three types, as briefly described below:

//.Figure 6 - DomiSMP UI: The Domain SML integration settings

[width="100%",cols="23%,52%,7%,18%",options="header"]
|===
|UC |Description |Operation |Data

4+h|Actor: `Group Admin`
a|*Create or Update resource:* +
`Service Group`
a|Create a new ServiceGroup for a new receiver participant.
This service stores the Service Group and links it to the specified pair: +
(`participantIdentifier` + `participantIndentifierScheme`)
the resource identifier.
Information is stored into Resource table.
This same service is used to create and update a `ServiceGroup`.
m|PUT
m|ServiceGroup

4+h|Actor: `Group SMP`
a|*Erase Service Group*
a|Erases the resource (service group definition) AND
the list of sub-resources such as service metadata for the
specified receiver participant.
m|DELETE
m|ServiceGroup

4+h|Actor: `Resource Admin`
a|*Create or Update Resource* +
such as Service Group Document and sub-resources: +
Service Metadata
a|Publish detailed information about one specific
document service (multiple processes and endpoints).
This same service is used to create and update ServiceMetaData.
m|PUT
m|ServiceMetadata

a|*Erase Service Metadata*
a|Remove all information about one specific
service (i.e. all related processes and endpoints definitions).
m|DELETE
m|ServiceMetadata

4+h|Actor:  `Anonymous User`
a|*Retrieve Service Group*
a|Obtain the list of public services
provided by a specific receiver participant (collection of references to
the ServiceMetaData's). This service provides the information related to
the Service Group according to the input pair: +
(`participantIdentifier`  + `participantIndentifierScheme`).
m|GET
m|ServiceGroup

a|Retrieve Service Metadata
a|Obtain detailed definition about one
specific service of a specific participant for all supported transports.
This service retrieves the SignedServiceMetadata according to the input
tuple: +
(`participantIdentifier` + `participantIndentifierScheme` + `documentIdentifier` + `documentIdentifierScheme`).
m|GET
m|SignedServiceMetadata

4+h|Actor: `System Admin`
a|
a|Create, modify, and delete users and domains. System
admin can be only used in the DomiSMP UI.
a|
a|
|===

NOTE: For a complete description of the SMP user management, please
consult the SMP Interface Control Document (ICD) document available at
`https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/SMP`.

Users can be added, modified and deleted using the SMP Admin console or
directly by executing sql commands. Below are instructions on how to
modify users in the dat abase.

=== BCRYPT Password Generation

To manage DomiSMP users, you can use the DomiSMP UI console.

Use the procedure below to create the first system admin user. +
An alternative is to use the provided SQL init scripts and replace passwords by first login.

The DomiSMP uses the BCRYPT algorithm to hash users' passwords.
A BCRYPT-hashing tool is bundled with the SMP WAR file.

To get the hashing code:

1. Place a copy of the `smp-X.war` file into a temporary directory.
2. Extract the war file using the `jar` command:
+
[source,bash]
----
jar -xvf smp-X.war
----

3. Obtain one or multiple hashes at once, using the following command:
+
[source,bash]
----
java -cp "WEB-INF/lib/*" eu.europa.ec.edelivery.smp.utils.BCryptPasswordHash <password_to_be_hashed>
----
+
The result is a BCRYPT hash of the specified password.
+
In the example below, `<password_to_be_hashed>` stands for `123456`.
+
._Example_
[source,bash]
----
java -cp "WEB-INF/lib/" eu.europa.ec.edelivery.smp.utils.BCryptPasswordHash 123456
----
+
._Result_
[source,bash]
----
2a$10$6nYTSUSh2BQfbOLIyCXn8eUViBcnn.WcjUrWOtJlMNDOdAtI85zMa
----
+
.Multiple Password Hashing
The next command shows the hashing of several passwords at once, separated by a space in the command.
+
[source,bash]
----
java -cp "WEB-INF/lib/" eu.europa.ec.edelivery.smp.BCryptPasswordHash <password_to_be_hashed_1> <password_to_be_hashed_2> 2a$10$6nYTSUSh2BQfbOLIyCXn8eUViBcnn.WcjUrWOtJlMNDOdAtI85zMa 2a$107zNzSeZpxiHeqY2BRKkHE.HknfIe3aiu6XzU.qHHnnPbUHKtfcmDG
----

=== SMP Database User Creation

Adding an SMP user is done by adding a new entry in the SMP database
`SMP_USER` table either directly or via the Administration console.

The User role is set in the SMP_USER table APPLICATION_ROLE column as
follows:

//.Figure 7 - Example of Domain/Group/Resource overwiew

[width="100%",cols="62%,38%",options="header"]
|===
|User Role |Role Value
a|System Administrator m|SYSTEM_ADMIN
a|DomiSMP User m|USER
a|Anonymous User a|N/A +
^*^_Not defined in the SMP user database_
|===

In the following examples, a *System Admin* user is created.

==== SYSTEM_ADMIN SMP User Creation

[NOTE]
====
To log in the Administration Console (for the first time),
it is necessary to, create a user with `SYSTEM_ADMIN` privileges by
entering the details directly into the `SMP_USER` table. +
This initial
user’s password is generated using the `BCRYPT` utility described
previously. +
If `PASSWORD_CHANGED` is not set, the user will be asked to change the
password at first logon.

Example of a `SYSTEM_ADMIN` user creation: +
*Username*: +
`smp_admin` +
*Password* (_hashed_): +
`$2a$10$6nYTSUSh2BQfbOLIyCXn8eUViBcnn.WcjUrWOtJlMNDOdAtI85zMa` +
*Role*: `SYSTEM_ADMIN`
====

Execute the following database command using the database user/password
created in the Database Configuration section of this guide.

.MySQL example:
[source, roomsql]
-----
INSERT INTO SMP_USER (USERNAME, ACTIVE, APPLICATION_ROLE, EMAIL, CREATED_ON, LAST_UPDATED_ON) VALUES (1,'smp_admin', 1, 'SYSTEM_ADMIN', 'system@mail-example.local', NOW(), NOW());

INSERT INTO SMP_CREDENTIAL (FK_USER_ID, CREDENTIAL_ACTIVE, CREDENTIAL_NAME, CREDENTIAL_VALUE, CREDENTIAL_TYPE, CREDENTIAL_TARGET, CREATED_ON, LAST_UPDATED_ON) VALUES ((SELECT id FROM SMP_USER WHERE USERNAME='smp_admin'), 1, 'smp_admin', '$2a$10$olcGeWKGEoRia2DPuFqRNeca0IEdRSmOrljLz57BAjf1jlC9SohrS', 'USERNAME_PASSWORD', 'UI', sysdate, sysdate);
-----

.Oracle example:
[source, roomsql]
-----
INSERT INTO SMP_USER (ID, USERNAME, ACTIVE, APPLICATION_ROLE, EMAIL, CREATED_ON, LAST_UPDATED_ON) VALUES (SMP_USER_SEQ.NEXTVAL,'smp_admin', 1, 'SYSTEM_ADMIN', 'system@mail-example.local', sysdate, sysdate);

INSERT INTO SMP_CREDENTIAL (FK_USER_ID, CREDENTIAL_ACTIVE, CREDENTIAL_NAME, CREDENTIAL_VALUE, CREDENTIAL_TYPE, CREDENTIAL_TARGET, CREATED_ON, LAST_UPDATED_ON) VALUES ((SELECT id FROM SMP_USER WHERE USERNAME='smp_admin'),1,'smp_admin', '$2a$10$olcGeWKGEoRia2DPuFqRNeca0IEdRSmOrljLz57BAjf1jlC9SohrS', 'USERNAME_PASSWORD','UI', sysdate, sysdate);
-----

NOTE: The username/password credential is stored in the `SMP_CREDENTIAL` table.

The record must have the following values set to:

* `CREDENTIAL_VALUE`: the BCrypted password
* `CREDENTIAL_TYPE`: value must be set to: 'USERNAME_PASSWORD'
* `CREDENTIAL_TARGET`: value must be set to: 'UI'
* `FK_USER_ID`: value must be set to user id.

