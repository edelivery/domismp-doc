== Localization

Currently, DomiSMP provides language support and date/time format per locale.

=== Translations

.DomiSMP UI Language Support
By default, DomiSMP UI ships in English, but it provides support for multiple languages.
This feature allows users to add custom translations.

[[smp_customtranslations]]
=== Setting Custom Translations
DomiSMP loads user provided translations from a pre-configured folder in its installation location.
The location of this folder is set via the `smp.locale.folder=locales` configuration property.

//For all DomiSMP Configuration Properties, see

This property takes as value either a relative or an absolute path to a directory accessible from the machine where DomiSMP is running on.

.Relative Paths to Translation Files
If relative, the provided path describes the file's location relative to the working directory of the server into which DomiSMP is deployed. If the directory does not yet exist, DomiSMP creates it along with all its parents when at startup.

.Multiple Translation Files
Multiple custom translations files can be deployed at the configured location. +
Translation files are `.json` files with names that are:

* prefixed with `ui_`;
* followed by the ISO 639 language code (`en`, `fr`, etc.).

DomiSMP automatically loads new valid translation files placed at the configured location.

.Creating a New Translation File

See below a part of the default `ui_en.json` file's:

.`ui_en.json`
[source,json]
----
{
  "column.selection.link.all": "All",
  "column.selection.link.none": "None",
  "column.selection.link.show": "Show columns",
  "column.selection.link.hide": "Hide columns",

  "cancel.dialog.text": "Do you want to cancel all unsaved operations?",
  "cancel.dialog.title": "Unsaved data",
}
----

When creating a new translation of the UI, copy the default `ui_en.json` file in order to reuse the labels referencing UI items, such as `column.selection.link.show` and replace the language values with its equivalents in the new language.

This new file should be named using the correct language code. See below a partial example for a French translation file. +
See also <<smp_supportedlocales>>.

.`ui_fr.json`
[source,json]
----
{
"column.selection.link.all": "Tout",
"column.selection.link.none": "Aucun",
"column.selection.link.show": "Afficher les colonnes",
"column.selection.link.hide": "Masquer les colonnes",

"cancel.dialog.text": "Voulez-vous annuler toutes les opérations non enregistrées ?",
"cancel.dialog.title": "Données non enregistrées",
}
----

[[smp_supportedlocales]]
.*DomiSMP Supported Locales*
[%collapsible]
====
[width="80%",cols="50%,50%",options="header", frame=none, grid="rows"]
|===

a|File a|Locale

a|`ui_bg.json` a|Bulgarian
a|`ui_cs.json` a|`Czech
a|`ui_da.json` a|Danish
a|`ui_de.json` a|German
a|`ui_el.json` a|Greek
a|`ui_en.json` a|English
a|`ui_es.json` a|Spanish
a|`ui_et.json` a|Estonian
a|`ui_fi.json` a|Finnish
a|`ui_fr.json` a|French
a|`ui_hr.json` a|Croatian
a|`ui_hu.json` a|Hungarian
a|`ui_it.json` a|Italian
a|`ui_lt.json` a|Lithuanian
a|`ui_lv.json` a|Latvian
a|`ui_mt.json` a|Maltese
a|`ui_nl.json` a|Dutch
a|`ui_pl.json` a|Polish
a|`ui_pt.json` a|Portuguese
a|`ui_ro.json` a|Romanian
a|`ui_sk.json` a|Slovak
a|`ui_sl.json` a|Slovenian
a|`ui_sv.json` a|Swedish
|===
====

=== Setting User's Language Preference

Once a translation file is created in the locales folder (see <<smp_customtranslations>>) and loaded in DomiSMP, users
can then set their preferred language by navigating to the *User Profile* page in the DomiSMP UI and selecting the
corresponding value from the dropdown. +

NOTE: The DomiSMP Console's Language preference option in *User Profile* is currently listing all European countries
official languages. +
If you would like to see other languages added on the dropdown, please contact eDelivery Support.

If a user selects a language for which a translation file cannot be found, DomiSMP then loads English, the default
translation file. +
This user preference is remembered between visits.

image::image20.png[width=1027,height=927]
