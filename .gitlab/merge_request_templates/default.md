### Description
<!-- 
Title Starts with Issue Key and Number and then one line to express what is the topic of the MR; Branch name is only the Jira issue key and number.

Commit message must start with ENNNNN, where E stands for "EDELIVERY" and "NNNNN" for the number of the issue. Example E12345.

Changes introduced by this merge request addresses, from the perspective of the end user. 
-->
### Affected Versions
<!-- This useful to understand and review the MR/changes. -->

### Merge Request Checklist
- [ ] Provides user-facing description
- [ ] Indicates affected versions

/draft
/assign @nevesdo @aebycar
/request_review @rihtajo @tincuse
/label ~NotToBeTested
